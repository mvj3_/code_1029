MKNetworkKit 是一个使用十分方便，功能又十分强大、完整的iOS网络编程代码库。它的目标是使用像AFNetworking这么简单，而功能像ASIHTTPRequest那么强大。它除了拥有AFNetworking和ASIHTTPRequest所有功能以外，还有一些新特色，包括：

    1. 自动缓存和恢复;

    2. 在整个app中可以只用一个队列（queue），队列的大小可以自动调整；

    3. 支持ARC；

    等等。